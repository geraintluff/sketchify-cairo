#include <cairo.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

// size of pencil lines, therefore minimum drawable feature
#define SKETCH_RESOLUTION 5
// integer denoting quality of pencil-effect for lines
#define SKETCH_PENCIL_EFFECT 1
// scale over which pencil-effect is drawn (higher numbers mean smoother textures)
#define SKETCH_PENCIL_EFFECT_SCALE 15
// opacity of single pencil stroke
#define SKETCH_ALPHA 0.05
// Cross-hatching amount for dull colours (mid-greys and blacks)
#define SKETCH_SHADING_SATURATION_CROSSHATCH 0.3
// Random shading angle
#define SKETCH_SHADING_ANGLE_RANDOM 0.3
// Quantise RGB to these levels (simulates blending colours from discrete number of pencils)
#define SKETCH_SHADING_LEVELS 4
// Overall shading density
#define SKETCH_SHADING_DENSITY 1
// cumulative colour distance at which to stop shading - higher values obscure more detail
#define SKETCH_SHADING_THRESHHOLD 0.3
// Length past which we draw lines more faintly - relative to resolution
#define SKETCH_SHADING_MAX_LENGTH 20
// Lighten the sketch (0 is neutral)
#define SKETCH_LIGHTNESS 0.5
// Saturate the sketch (0 is neutral)
#define SKETCH_SATURATION 0.6

#define SKETCH_SHADING_CURLY 1

typedef struct rgba_float {
    double alpha;
    double red;
    double green;
    double blue;
} rgba_float;

typedef struct xy_float {
    double x;
    double y;
} xy_float;

xy_float randomPair() {
    double u = rand()*2.0/(double)RAND_MAX - 1, v = rand()*2.0/(double)RAND_MAX - 1;
    double s = u*u + v*v;
    while (!s || s >= 1) {
        u = rand()*2.0/(double)RAND_MAX - 1, v = rand()*2.0/(double)RAND_MAX - 1;
        s = u*u + v*v;
    }
    double norm = sqrt(-2*log(s)/s);
    xy_float result = {u*norm, v*norm};
    return result;
}

xy_float _pencilPairCurrent = {0, 0};
xy_float pencilPair(double distance) {
    double factor = (double)distance/(double)SKETCH_RESOLUTION/(double)SKETCH_PENCIL_EFFECT_SCALE;
    double boostFactor = sqrt(2/factor - 1);
    xy_float random = randomPair();
    _pencilPairCurrent.x += (random.x*boostFactor - _pencilPairCurrent.x)*factor;
    _pencilPairCurrent.y += (random.y*boostFactor - _pencilPairCurrent.y)*factor;
    return _pencilPairCurrent;
}

double pixel_distance(rgba_float *a, rgba_float *b) {
    double red = a->red - b->red;
    double green = a->green - b->green;
    double blue = a->blue - b->blue;
    return sqrt(red*red + green*green + blue*blue);
}

void get_pixel(cairo_surface_t *surface, double x, double y, rgba_float *pixel) {
    int width = cairo_image_surface_get_width(surface);
    int height = cairo_image_surface_get_height(surface);
    int stride = cairo_image_surface_get_stride(surface);
    cairo_format_t format = cairo_image_surface_get_format(surface);

    int intX = (x < 0) ? 0 : (x >= width - 1) ? width - 1 : lround(x);
    int intY = (y < 0) ? 0 : (y >= height - 1) ? height - 1 : lround(y);

    unsigned char *surfaceData = cairo_image_surface_get_data(surface);
    unsigned char *rowData = surfaceData + intY*stride;
    if (format == CAIRO_FORMAT_ARGB32) {
        unsigned char *pixelData = rowData + intX*4; // ARGB
        pixel->alpha = pixelData[3]/255.0f;
        pixel->red = pixelData[2]/255.0f;
        pixel->green = pixelData[1]/255.0f;
        pixel->blue = pixelData[0]/255.0f;
    } else if (format == CAIRO_FORMAT_RGB24) {
        unsigned char *pixelData = rowData + intX*4; // ARGB
        pixel->alpha = 1;
        pixel->red = pixelData[2]/255.0f;
        pixel->green = pixelData[1]/255.0f;
        pixel->blue = pixelData[0]/255.0f;
    } else {
        pixel->red = pixel->blue = pixel->green = 0.5;
        pixel->alpha = 1;
        return;
    }
}

void shading_direction(rgba_float *pixel, xy_float *direction) {
    // Calcuations for hue
    double diff1 = sqrt(3)*(pixel->green - pixel->blue);
    double diff2 = 2*pixel->red - pixel->green - pixel->blue;

    double maxRgb = fmax(pixel->red, fmax(pixel->green, pixel->blue));
    double minRgb = fmin(pixel->red, fmin(pixel->green, pixel->blue));
    double dullness = minRgb*(1 - maxRgb)/(maxRgb + 1e-6);

    double tmp1 = diff1, tmp2 = diff2;
    double crossHatchAmount = SKETCH_SHADING_SATURATION_CROSSHATCH*dullness
        + (rand()/(double)RAND_MAX - 0.5)*SKETCH_SHADING_ANGLE_RANDOM;
    if (rand() < RAND_MAX/2) {
        crossHatchAmount = -crossHatchAmount;
    }
    diff1 += crossHatchAmount*tmp2;
    diff2 -= crossHatchAmount*tmp1;

    double mag = sqrt(diff1*diff1 + diff2*diff2);
    if (!mag) { // Instead of atan() and reproducing, just normalise to unit
        direction->x = 1;
        direction->y = 0;
    } else {
        direction->y = diff1/mag;
        direction->x = diff2/mag;
    }
    return;
}

cairo_surface_t * sketch_surface(cairo_surface_t *original) {
    unsigned char *originalData = cairo_image_surface_get_data(original);
    int width = cairo_image_surface_get_width(original);
    int height = cairo_image_surface_get_height(original);
    int stride = cairo_image_surface_get_stride(original);

    int strokeTotalLength = 0;
    int strokeTargetLength = width*height*SKETCH_SHADING_DENSITY/SKETCH_RESOLUTION/SKETCH_ALPHA;

    cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cairo_t *context = cairo_create(surface);
    cairo_set_source_rgba(context, 1, 1, 1, 1);
    cairo_rectangle(context, 0, 0, width, height);
    cairo_fill(context);

    cairo_set_line_cap(context, CAIRO_LINE_CAP_BUTT);

    cairo_surface_flush(original);
    while (strokeTotalLength < strokeTargetLength) {
        rgba_float referencePixel;
        xy_float direction;
        double referenceX = rand()/(double)RAND_MAX*width - 0.5;
        double referenceY = rand()/(double)RAND_MAX*height - 0.5;
        get_pixel(original, referenceX, referenceY, &referencePixel);
        shading_direction(&referencePixel, &direction);

        double increment = 0.5*SKETCH_RESOLUTION;
        double colourDistanceLimit = SKETCH_RESOLUTION*SKETCH_SHADING_THRESHHOLD;
        colourDistanceLimit *= rand()/(double)RAND_MAX + rand()/(double)RAND_MAX; // Triangle distribution [0, 1, 2]
        double colourDistance;

        int maxIncrements = 200;
        int endCoordCounter = 0, startCoordCounter = 0;
        double endXCoords[200], endYCoords[200];
        double startXCoords[200], startYCoords[200];

        double endX = referenceX, endY = referenceY;
        colourDistance = 0;
        rgba_float endPixel = referencePixel;
        do {
            // Store point
            endXCoords[endCoordCounter] = endX;
            endYCoords[endCoordCounter] = endY;
            endCoordCounter++;
            if (endCoordCounter >= maxIncrements) break;

            if (SKETCH_SHADING_CURLY) shading_direction(&endPixel, &direction);

            endX += direction.x*increment;
            endY += direction.y*increment;
            get_pixel(original, endX, endY, &endPixel);
            colourDistance += pixel_distance(&endPixel, &referencePixel)*increment;
        } while (endX > 0 && endX < width && endY > 0 && endY < height && colourDistance < colourDistanceLimit);

        double startX = referenceX, startY = referenceY;
        colourDistance = 0;
        rgba_float startPixel = referencePixel;
        do {
            // Store point
            startXCoords[startCoordCounter] = startX;
            startYCoords[startCoordCounter] = startY;
            startCoordCounter++;
            if (startCoordCounter >= maxIncrements) break;

            if (SKETCH_SHADING_CURLY) shading_direction(&startPixel, &direction);

            startX -= direction.x*increment;
            startY -= direction.y*increment;
            get_pixel(original, startX, startY, &startPixel);
            colourDistance += pixel_distance(&startPixel, &referencePixel)*increment;
        } while (startX > 0 && startX < width && startY > 0 && startY < height && colourDistance < colourDistanceLimit);

        double alpha = SKETCH_ALPHA;
        double lineLength = sqrt((startX - endX)*(startX - endX) + (startY - endY)*(startY - endY));
        double maxLength = SKETCH_RESOLUTION*SKETCH_SHADING_MAX_LENGTH;
        if (lineLength > maxLength) {
            alpha *= maxLength/lineLength;
            lineLength = maxLength;
        }

        double averageRgb = (referencePixel.red + referencePixel.green + referencePixel.blue)/3.0;
        referencePixel.red += (referencePixel.red - averageRgb)*SKETCH_SATURATION;
        referencePixel.green += (referencePixel.green - averageRgb)*SKETCH_SATURATION;
        referencePixel.blue += (referencePixel.blue - averageRgb)*SKETCH_SATURATION;

        double minRgb = fmin(referencePixel.red, fmin(referencePixel.green, referencePixel.blue));
        double darkness = 1 - minRgb;
        double drawProb = pow(1 - averageRgb, SKETCH_LIGHTNESS);
        if (darkness > 0 && rand() < RAND_MAX*darkness*drawProb) {
            referencePixel.red = 1 - (1 - referencePixel.red)/darkness;
            referencePixel.green = 1 - (1 - referencePixel.green)/darkness;
            referencePixel.blue = 1 - (1 - referencePixel.blue)/darkness;

            // Quantise to different levels
            referencePixel.red = lround(referencePixel.red*SKETCH_SHADING_LEVELS + rand()/(double)RAND_MAX - 0.5)/(double)SKETCH_SHADING_LEVELS;
            referencePixel.green = lround(referencePixel.green*SKETCH_SHADING_LEVELS + rand()/(double)RAND_MAX - 0.5)/(double)SKETCH_SHADING_LEVELS;
            referencePixel.blue = lround(referencePixel.blue*SKETCH_SHADING_LEVELS + rand()/(double)RAND_MAX - 0.5)/(double)SKETCH_SHADING_LEVELS;

            // Draw line - start points in reverse order
            double singleLineWidth = SKETCH_RESOLUTION/sqrt(SKETCH_PENCIL_EFFECT);
            cairo_set_line_width(context, singleLineWidth);
            cairo_set_source_rgba(context, referencePixel.red, referencePixel.green, referencePixel.blue, alpha/sqrt(SKETCH_PENCIL_EFFECT));
            double targetVariance = SKETCH_RESOLUTION*SKETCH_RESOLUTION/12.0;
            double singleLineVariance = singleLineWidth*singleLineWidth/12;
            double randomSd = sqrt(targetVariance - singleLineVariance)*2;
            randomSd = fmax(0, randomSd);

            for (int pencilIteration = 0; pencilIteration < SKETCH_PENCIL_EFFECT; pencilIteration++) {
                cairo_new_path(context);
                xy_float random = pencilPair(increment);
                cairo_move_to(context, startXCoords[startCoordCounter - 1] + random.x*randomSd, startYCoords[startCoordCounter - 1] + random.y*randomSd);
                for (int i = startCoordCounter - 2; i >= 0; i--) {
                    xy_float random = pencilPair(increment);
                    cairo_line_to(context, startXCoords[i] + random.x*randomSd, startYCoords[i] + random.y*randomSd);
                }
                for (int i = 1; i < endCoordCounter; i++) {
                    xy_float random = pencilPair(increment);
                    cairo_line_to(context, endXCoords[i] + random.x*randomSd, endYCoords[i] + random.y*randomSd);
                }
                cairo_stroke(context);
            }
        }
        strokeTotalLength += lineLength;
    }

    cairo_surface_flush(surface);
    cairo_destroy(context);
    return surface;
}

int main (int argc, char *argv[]) {
    srand(clock());
    if (argc < 2) {
        printf("Usage: program_name <file_in> <file_out>\n");
        exit(1);
    }

    cairo_surface_t *original = cairo_image_surface_create_from_png(argv[1]);
    if (!original) {
        printf("Failed to load image\n");
        exit(1);
    }
    cairo_surface_t *sketch = sketch_surface(original);
    if (!sketch) {
        printf("Failed to sketch image\n");
        exit(1);
    }

    char outputFile[512];
    if (argc >= 3) {
        strcpy(outputFile, argv[2]);
    } else {
        strcpy(outputFile, argv[1]);
        strcat(outputFile, ".sketch.png");
    }

    cairo_status_t writeStatus = cairo_surface_write_to_png(sketch, outputFile);
    cairo_surface_destroy(original);
    cairo_surface_destroy(sketch);
    if (writeStatus == CAIRO_STATUS_SUCCESS) {
        return 0;
    } else {
        printf("Write failed: %s\n", cairo_status_to_string(writeStatus));
        return writeStatus;
    }
}
